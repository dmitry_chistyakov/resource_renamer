package renamer;

import com.android.ide.common.vectordrawable.Svg2Vector;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class Renamer {

    public static final String MDPI = "mdpi";
    public static final String HDPI = "hdpi";
    public static final String XHDPI = "xhdpi";
    public static final String XXHDPI = "xxhdpi";
    public static final String XXXHDPI = "xxxhdpi";

    private static File dir;

    public static void main(String[] args) {

        if (args == null || args.length == 0) {
            System.out.println("Please specify resource folder as first and only parameter");
            return;
        }

        String path = args[0];
        dir = new File(path);
        if (!dir.exists() || !dir.isDirectory()) {
            System.out.println("Path: " + path + " is not valid directory");
            return;
        }

        int skipped = 0;
        int copied = 0;
        int deleted = 0;

        for (String fileName : dir.list(null)) {
            File file = new File(dir, fileName);

            if (file.isDirectory()) {
                continue;
            }

            if (file.getName().toLowerCase().endsWith(".svg")) {
                int[] ints = importSvg(file);
                copied += ints[0];
                deleted += ints[1];
                continue;
            }

            if (fileName.contains("@" + MDPI) ||
                    fileName.contains("@" + HDPI) ||
                    fileName.contains("@" + XHDPI) ||
                    fileName.contains("@" + XXHDPI) ||
                    fileName.contains("@" + XXXHDPI)) {

                File targetFolder = null;
                String targetName = null;

                if (fileName.contains("@" + MDPI)) {
                    targetFolder = new File(dir, "drawable-" + MDPI);
                    targetName = fileName.replace("@" + MDPI, "");
                } else if (fileName.contains("@" + HDPI)) {
                    targetFolder = new File(dir, "drawable-" + HDPI);
                    targetName = fileName.replace("@" + HDPI, "");
                } else if (fileName.contains("@" + XHDPI)) {
                    targetFolder = new File(dir, "drawable-" + XHDPI);
                    targetName = fileName.replace("@" + XHDPI, "");
                } else if (fileName.contains("@" + XXHDPI)) {
                    targetFolder = new File(dir, "drawable-" + XXHDPI);
                    targetName = fileName.replace("@" + XXHDPI, "");
                } else if (fileName.contains("@" + XXXHDPI)) {
                    targetFolder = new File(dir, "drawable-" + XXXHDPI);
                    targetName = fileName.replace("@" + XXXHDPI, "");
                }
                if (!targetFolder.exists()) {
                    System.out.println("file: " + file.getName() + " skipped, target folder: " + targetFolder.getName() + " not exists");
                    skipped++;
                    continue;
                }
                File target = new File(targetFolder, targetName);
                try {
                    Files.move(file.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    System.out.println("file: " + file.getName() + " moved");
                    copied++;
                } catch (Exception ex) {
                    System.out.println("file: " + file.getName() + " skipped, error: " + ex.getMessage());
                    skipped++;
                }
            } else {
                System.out.println("file: " + file.getName() + " skipped, name mismatch");
                skipped++;
            }
        }

        System.out.println("done, files moved: " + copied + ",\nskipped: " + skipped + ",\ndeleted: " + deleted);
    }

    private static int[] importSvg(File svgFile) {
        System.out.println("found svg file: " + svgFile.getName());
        File xmlFile = null;
        try {
            xmlFile = convertSvgToXml(svgFile);
        } catch (IOException e) {
            System.out.println("failed to convert svg: " + e.getMessage());
            return new int[]{0, 0};
        }
        try {
            File targetPath = new File(dir, "drawable");
            File targetFile = new File(targetPath, xmlFile.getName());
            Files.move(xmlFile.toPath(), targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new int[] {0, 0};
        }
        svgFile.delete();
        String pngFile = svgFile.getName().replace(".svg", ".png");
        int deleted = deleteOldPng(pngFile);
        return new int[]{1, deleted};
    }
    private static File convertSvgToXml(File file) throws IOException {
        File output = new File(file.getParentFile().getPath(), file.getName().replace(".svg", ".xml"));
        OutputStream outputStream = new FileOutputStream(output);
        Svg2Vector.parseSvgToXml(file, outputStream);
        outputStream.close();
        return output;
    }

    private static int deleteOldPng(String fileName) {
        int deleted = 0;
        File drawableDir = new File(dir, "drawable-" + MDPI);
        if (new File(drawableDir, fileName).delete()) {
            deleted++;
        }
        drawableDir = new File(dir, "drawable-" + HDPI);
        if (new File(drawableDir, fileName).delete()) {
            deleted++;
        }
        drawableDir = new File(dir, "drawable-" + XHDPI);
        if (new File(drawableDir, fileName).delete()) {
            deleted++;
        }
        drawableDir = new File(dir, "drawable-" + XXHDPI);
        if (new File(drawableDir, fileName).delete()) {
            deleted++;
        }
        drawableDir = new File(dir, "drawable-" + XXXHDPI);
        if (new File(drawableDir, fileName).delete()) {
            deleted++;
        }
        System.out.println(deleted + " old png deleted");
        return deleted;
    }

}
